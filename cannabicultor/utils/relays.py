from datetime import datetime
from logging import getLogger

import structlog

from gpiozero import DigitalOutputDevice

LOG_MESSAGE = "{output_type} {action} at: {timestamp}"

logger = structlog.get_logger(__name__)

class Relay(DigitalOutputDevice):
    def __init__(self, order: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.order = order

    def __str__(self) -> str:
        return f"<Relay {self.order}>"

    def on(self):
        logger.info(
            LOG_MESSAGE.format(
                output_type=self,
                action="ON",
                timestamp=datetime.now(),
            )
        )
        super().on()

    def off(self):
        logger.info(
            LOG_MESSAGE.format(
                output_type=self,
                action="OFF",
                timestamp=datetime.now(),
            )
        )
        super().off()
