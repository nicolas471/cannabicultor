from pathlib import Path

import yaml


def load_config() -> dict:
    config_path = Path("config.yaml")
    with config_path.open() as file:
        return yaml.safe_load(file.read())
