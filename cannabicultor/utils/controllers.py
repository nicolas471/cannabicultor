import asyncio
from datetime import datetime
from typing import List, NoReturn

import structlog
from croniter import croniter

from cannabicultor.utils.relays import Relay
from cannabicultor.utils.utils import load_config

logger = structlog.get_logger(__name__)

START = "start"
STOP = "stop"
ONOFF = "onoff"
TOGGLE = "toggle"
CONTROLLER_TYPES = (ONOFF, TOGGLE)

REL1 = Relay(order=1, pin=4)
REL2 = Relay(order=2, pin=22)
REL3 = Relay(order=3, pin=6)
REL4 = Relay(order=4, pin=26)

RELAYS = [REL1, REL2, REL3, REL4]


class InexistentRelay(Exception):
    pass


class Controller:
    """Abstract class for model relays controller"""

    controller_type: str
    relay: Relay

    def __init__(self, order: int, config: dict, base_datetime=None) -> None:
        self.base_datetime = base_datetime or datetime.now()
        self.configure(order, config)

    def get_relay(self, order: int) -> Relay:
        try:
            return next(r for r in RELAYS if r.order == order)
        except:
            raise InexistentRelay()

    def configure(self, order: int, config: dict) -> None:
        """Use for configure the controller"""
        raise NotImplementedError("Boom!")

    async def run(self) -> NoReturn:
        """Base method for control logic"""
        raise NotImplementedError("Boom!")


class ToggleController(Controller):
    controller_type = TOGGLE
    toggle_cron: croniter

    def configure(self, order: int, config: dict):
        exp = config[TOGGLE]
        self.toggle_cron = croniter(exp, self.base_datetime, ret_type=datetime)
        self.relay = self.get_relay(order)
        logger.info(
            f"Configured {self.relay} with {self.controller_type.upper()} [{exp}]"
        )

    async def run(self) -> NoReturn:
        while True:
            next_event = self.toggle_cron.get_next()
            logger.debug(f"Next {TOGGLE} for {self.relay} at: {next_event}")
            await asyncio.sleep((next_event - datetime.now()).seconds)
            self.relay.toggle()


class OnOffController(Controller):
    controller_type = ONOFF
    start_cron: croniter
    stop_cron: croniter

    def should_be_on(self, next_start: datetime, next_stop: datetime) -> bool:
        return next_start > next_stop

    def configure(self, order: int, config: dict) -> None:
        start_exp = config["start"]
        stop_exp = config["end"]

        self.start_cron = croniter(start_exp, self.base_datetime, ret_type=datetime)
        self.stop_cron = croniter(stop_exp, self.base_datetime, ret_type=datetime)
        self.relay = self.get_relay(order)
        logger.info(
            f"Configured {self.relay} with {self.controller_type.upper()} "
            f"[START: {start_exp} / STOP: {stop_exp}]"
        )

    async def run(self) -> NoReturn:
        next_start = self.start_cron.get_next()
        next_stop = self.stop_cron.get_next()

        if self.should_be_on(next_start, next_stop):
            self.relay.on()

        while True:
            if next_start < next_stop:
                # wait for start
                logger.debug(f"Next {START} for {self.relay} at: {next_start}")
                await asyncio.sleep((next_start - datetime.now()).seconds)
                self.relay.on()
                next_start = self.start_cron.get_next()

            if next_start > next_stop:
                # wait for stop
                logger.debug(f"Next {STOP} for {self.relay} at: {next_stop}")
                await asyncio.sleep((next_stop - datetime.now()).seconds)
                self.relay.off()
                next_stop = self.stop_cron.get_next()


available_controllers = [ToggleController, OnOffController]


def load_controllers() -> List[Controller]:
    config = load_config()
    controllers = []

    for configs in config["relays"]:
        for order, config in configs.items():
            controller_type = config.pop("type")
            controller_class = next(
                c for c in available_controllers if c.controller_type == controller_type
            )
            controllers.append(controller_class(order, config))
    return controllers


async def run():
    controllers = load_controllers()
    await asyncio.gather(*[controller.run() for controller in controllers])
