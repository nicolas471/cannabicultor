import asyncio
import sys

import typer
import structlog

from cannabicultor.utils import settings, controllers

logger = structlog.get_logger(__name__)

def deamon():
    logger.info(f"Deamon started at {settings.INIT_DATETIME}")
    asyncio.run(controllers.run())

def main():
    try:
        typer.run(deamon)
    except KeyboardInterrupt as e:
        print("Program exit")
        sys.exit()