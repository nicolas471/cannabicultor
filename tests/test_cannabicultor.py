import pytest
from freezegun import freeze_time
from cannabicultor.utils.controllers import OnOffController, ToggleController
from unittest.mock import patch


@pytest.fixture
def onoff_config():
    return {"start": "0 18 * * *", "end": "0 8 * * *"}


@pytest.fixture
def order():
    return 1


@freeze_time("2021-07-14 12:00:00", tick=True)
def test_onoff_controller_should_be_off(order, onoff_config):
    controller = OnOffController(order, onoff_config)
    # breakpoint()
    assert not controller.should_be_on(
        controller.start_cron.get_next(), controller.stop_cron.get_next()
    )


@freeze_time("2021-07-14 18:01:00")
def test_onoff_controller_should_be_on(order, onoff_config):
    controller = OnOffController(order, onoff_config)
    assert controller.should_be_on(
        controller.start_cron.get_next(), controller.stop_cron.get_next()
    )
